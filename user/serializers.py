def __url(field):
    return field.url


def __plain(field):
    return field


def __paintings(field):
    return [str(painting.id) for painting in x]


user_serializer = {
    'id': str,
    'username': __plain,
    'avatar50': __url,
    'avatar200': __plain,
    'name': __plain,
    'surname': __plain,
    'email': __plain,
    'level': __plain,
    'experience': __plain,
    'experience_to_next_level': __plain,
    'total_kilometers': __plain,
    'completed_paintings': __paintings,
    'paused_paintings': __paintings,
}