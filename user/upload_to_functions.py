def upload_avatar50(instance, filename):
    file_extension = filename.rsplit('.', 1)[-1]
    return f'user/{instance.username}/avatar50.{file_extension}'


def upload_avatar200(instance, filename):
    file_extension = filename.rsplit('.', 1)[-1]
    return f'user/{instance.username}/avatar200.{file_extension}'


def upload_geolocations(instance, filename):
    return f'user/{instance.username}/paintings/{instance.current_painting.id}.json'


def upload_completed_painting_geolocations(instance, filename):
    return f'user/{instance.user.username}/paintings/completed/{instance.painting.id}.json'


def upload_paused_painting_geolocations(instance, filename):
    return f'user/{instance.user.username}/paintings/paused/{instance.painting.id}.json'
