from django.db import models
from django.utils import timezone
from django.core.files.base import ContentFile
from django.contrib.auth.models import AbstractBaseUser
from django.core.validators import validate_email
from django.core.files import File
from datetime import datetime
from PIL import Image
from io import BytesIO
import uuid

from .upload_to_functions import (
    upload_avatar50, upload_avatar200, upload_geolocations,
    upload_completed_painting_geolocations,
    upload_paused_painting_geolocations
)
from storages import private_storage, avatar_storage


def create_thumbnails(avatar):
    avatar_img = Image.open(avatar)
    avatar_copy = avatar_img.copy()
    avatar_copy.thumbnail((50, 50))
    file_bytes = BytesIO()
    avatar_copy.save(file_bytes)
    avatar50 = File(file_bytes, name=avatar_img.name)
    avatar_copy = avatar_img.copy()
    avatar_copy.thumbnail((200, 200))
    file_bytes = BytesIO()
    avatar_copy.save(file_bytes)
    avatar200 = File(file_bytes, name=avatar_img.name)
    return avatar50, avatar200


class User(AbstractBaseUser):
    USERNAME_FIELD = 'username'

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, unique=True)
    username = models.CharField(max_length=120, unique=True, db_index=True)
    avatar50 = models.ImageField(upload_to=upload_avatar50, null=True, storage=avatar_storage)
    avatar200 = models.ImageField(upload_to=upload_avatar200, null=True, storage=avatar_storage)
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    email = models.CharField(max_length=120, validators=[validate_email])
    level = models.PositiveIntegerField(default=0)
    experience = models.PositiveIntegerField(default=0)
    experience_to_next_level = models.PositiveIntegerField(default=200)
    total_kilometers = models.FloatField(default=0.0)
    completed_paintings = models.ManyToManyField(
        'painting.Painting',
        related_name='completed_by',
        through='CompletedPainting',
        through_fields=('user', 'painting')
    )
    paused_paintings = models.ManyToManyField(
        'painting.Painting',
        related_name='paused_by',
        through='PausedPainting',
        through_fields=('user', 'painting')
    )
    current_painting = models.ForeignKey('painting.Painting', models.SET_NULL, null=True)
    current_geolocations = models.FileField(
        upload_to=upload_geolocations,
        storage=private_storage,
        null=True
    )
    refresh_token = models.CharField(max_length=86, null=True, db_index=True)
    refresh_token_expiration = models.DateTimeField(null=True)

    def __init__(self, *args, **kwargs):
        if 'avatar' in kwargs:
            kwargs['avatar50'], kwargs['avatar200'] = create_thumbnails(kwargs['avatar'])
            del kwargs['avatar']
        super(User, self).__init__(*args, **kwargs)


class CompletedPainting(models.Model):
    user = models.ForeignKey('User', models.CASCADE)
    painting = models.ForeignKey('painting.Painting', models.CASCADE)
    time = models.DateTimeField(default=timezone.now)
    geolocations = models.FileField(
        upload_to=upload_completed_painting_geolocations,
        storage=private_storage
    )


class PausedPainting(models.Model):
    user = models.ForeignKey('User', models.CASCADE)
    painting = models.ForeignKey('painting.Painting', models.CASCADE)
    time = models.DateTimeField(default=timezone.now)
    geolocations = models.FileField(
        upload_to=upload_paused_painting_geolocations,
        storage=private_storage
    )
