from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.db.models import Model

from utilities import json_request
from .serializers import user_serializer
from .models import User


@json_request
def get_update_user(request, user_id):
    json_data = request.json_data
    try:
        user = User.objects.get(id=user_id)
    except Model.DoesNotExist:
        return JsonResponse({'error': 'not_found'}, status=404)
    if request.method == 'GET':
        if 'fields' in json_data and type(json_data['fields']) == list:
            keys = set(user_serializer.keys()) & set(json_data['fields'])
        else:
            keys = set(user_serializer.keys())
        resp = {}
        for key in keys:
            resp[key] = user_serializer[key](user.__getattribute__(key))
        return JsonResponse(resp)
    elif request.method == 'PUT':
        # TODO добавить проверку jwt
        if 'user' not in json_data or \
                type(json_data['user']) != dict or \
                ('password' in json_data and 'repeat_password' not in json_data) or \
                ('password' not in json_data and 'repeat_password' in json_data):
            return JsonResponse({'error': 'bad_request'}, status=400)
        user_data = json_data['user']
        for key in user_data:
            if key == 'password':
                if user_data['password'] != user_data['repeat_password']:
                    return JsonResponse({'error': 'passwords_do_not_match'}, status=400)
                else:
                    user.set_password(user_data['password'])
            if key == 'repeat_password':
                pass
            else:
                user.__setattr__(key, user_data[key])
        try:
            user.save()
        except ValidationError:
            return JsonResponse({'error': 'input_invalid'}, status=400)
        return JsonResponse({'result': 'ok'})
    else:
        return JsonResponse({'error': 'method_not_allowed'}, status=405)


@json_request
def create_user(request):
    json_data = request.json_data
    if request.method == 'POST':
        if 'user' not in json_data:
            return JsonResponse({'error': 'bad_request'}, status=400)
        user_data = json_data['user']
        if 'password' not in user_data or 'repeat_password' not in user_data:
            return JsonResponse({'error': 'bad_request'}, status=400)
        password = user_data['password']
        del user_data['password']
        del user_data['repeat_password']
        try:
            user = User(**user_data)
        except (ValidationError, TypeError):
            return JsonResponse({'error': 'input_invalid'}, status=405)
        user.set_password(password)
        user.save()
        return JsonResponse({'result': 'ok'})
    else:
        return JsonResponse({'error': 'method_not_allowed'}, status=405)
