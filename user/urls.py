from django.urls import path

from .views import get_update_user, create_user

app_name = 'user'
urlpatterns = [
    path('', create_user),
    path('<str:user_id>/', get_update_user)
]
