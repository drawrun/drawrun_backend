from datetime import datetime, timedelta
from django.utils import timezone
from django.conf import settings
import secrets
import jwt

from user.models import User


def authenticate(user):
    expires_at = (datetime.utcnow() - datetime(1970, 1, 1) + timedelta(minutes=15)).total_seconds()
    access_token = jwt.encode(
        {'sub': str(user.id), 'exp': expires_at},
        settings.SECRET_KEY,
        algorithm='HS256'
    ).decode('utf-8')
    print(type(access_token))
    refresh_token = secrets.token_urlsafe(64)
    user.refresh_token = refresh_token
    user.refresh_token_expiration = datetime.utcnow() + timedelta(days=30)
    user.save()
    return access_token, refresh_token


def authorize(access_token):
    return jwt.decode(access_token, settings.SECRET_KEY, algorithms=['HS256'])['sub']


def refresh(refresh_token):
    user = User.objects.get(refresh_token=refresh_token)
    if timezone.now() > user.refresh_token_expiration:
        raise ValueError
    return authenticate(user)
