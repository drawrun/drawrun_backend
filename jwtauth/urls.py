from django.urls import path

from .views import login_view, logout_view, refresh_view

app_name = 'jwtauth'
urlpatterns = [
    path('login/', login_view),
    path('logout/', logout_view),
    path('refresh/', refresh_view),
]
