from django.test import TestCase

from user.models import User
from painting.tests.jsonclient import JsonClient


class JwtauthTestCase(TestCase):
    client_class = JsonClient

    def setUp(self):
        self.user = User(
            username='Goshan',
            name='Georgiy',
            surname='Surkov',
            email='georgiy@example.com'
        )
        self.password = 'jkvnsefms7234!'
        self.user.set_password(self.password)
        self.user.save()

    def test_login_username(self):
        response = self.client.json_post(
            '/account/login/',
            {
                'login': self.user.username,
                'password': self.password
            }
        )
        self.assertEqual(True, 'access_token' in response)

    def test_login_email(self):
        response = self.client.json_post(
            '/account/login/',
            {
                'login': self.user.email,
                'password': self.password
            }
        )
        self.assertEqual(True, 'access_token' in response)

    def test_refresh_token(self):
        refresh_token = self.client.json_post(
            '/account/login/',
            {
                'login': self.user.email,
                'password': self.password
            }
        )['refresh_token']
        response = self.client.json_post(
            '/account/refresh/',
            {
                'refresh_token': refresh_token
            }
        )
        self.assertEqual(True, 'access_token' in response and 'refresh_token' in response)
