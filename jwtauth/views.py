from django.http import JsonResponse
from django.db.models import ObjectDoesNotExist
import jwt

from utilities import json_request
from user.models import User
from .services import authenticate, authorize, refresh


@json_request
def login_view(request):
    json_data = request.json_data
    if not ('login' in json_data and 'password' in json_data):
        return JsonResponse({'error': 'bad_request'}, status=400)
    try:
        user = User.objects.get(username=json_data['login'])
    except ObjectDoesNotExist:
        try:
            user = User.objects.get(email=json_data['login'])
        except ObjectDoesNotExist:
            return JsonResponse({'error': 'no_user_with_this_login'}, status=401)
    if not user.check_password(json_data['password']):
        return JsonResponse({'error': 'password_incorrect'}, status=401)
    access_token, refresh_token = authenticate(user)
    return JsonResponse({'access_token': access_token, 'refresh_token': refresh_token})


@json_request
def logout_view(request):
    json_data = request.json_data
    # TODO добавлять access_token в базу данных, как не валидный
    if 'access_token' not in json_data:
        return JsonResponse({'error': 'unauthorized'}, status=401)
    access_token = json_data['access_token']
    try:
        user = User.objects.get(id=authorize(access_token))
    except (jwt.DecodeError, jwt.InvalidSignatureError):
        return JsonResponse({'error': 'invalid_token'}, status=401)
    except jwt.ExpiredSignatureError:
        return JsonResponse({'error': 'token_expired'}, status=401)
    user.refresh_token = None
    user.refresh_token_expiration = None
    return JsonResponse({'status': 'ok'})


@json_request
def refresh_view(request):
    json_data = request.json_data
    if 'refresh_token' not in json_data:
        return JsonResponse({'error': 'unauthorized'}, status=401)
    refresh_token = json_data['refresh_token']
    try:
        access_token, refresh_token = refresh(refresh_token)
    except User.DoesNotExist:
        return JsonResponse({'error': 'unauthorized'}, status=401)
    except ValueError:
        return JsonResponse({'error': 'token_expired'}, status=401)
    return JsonResponse({'access_token': access_token, 'refresh_token': refresh_token})
