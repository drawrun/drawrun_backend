from django.http import JsonResponse
import json


def json_request(view):
    def inner(request, *args, **kwargs):
        try:
            request.json_data = json.loads(request.body.decode(encoding='utf-8'))
        except json.JSONDecodeError:
            return JsonResponse({'error': 'bad_request'}, status=400)
        return view(request, *args, **kwargs)
    return inner
