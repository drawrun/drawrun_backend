import json


def __plain(field):
    return field


def __url(field):
    return field.url


def __file(field):
    with field.open() as f:
        return json.load(f)


painting_serializer = {
    'id': str,
    'name': __plain,
    'experience': __plain,
    'thumbnail50': __url,
    'thumbnail200': __url,
    'author': lambda field: str(field.id),
    'path': __file,
    'rating': __plain,
    'lat': __plain,
    'lon': __plain,
}
