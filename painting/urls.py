from django.urls import path

from .views import (
    get_local_popular,
    get_put_delete,
    start,
    send_geolocations,
    pause,
    continue_painting,
    finish,
    create
)

app_name = 'painting'
urlpatterns = [
    path('get_local_popular/', get_local_popular),
    path('<str:painting_id>/', get_put_delete),
    path('<str:painting_id>/start/', start),
    path('<str:painting_id>/send_geolocations/', send_geolocations),
    path('<str:painting_id>/pause/', pause),
    path('<str:painting_id>/continue/', continue_painting),
    path('<str:painting_id>/finish/', finish),
    path('', create)
]
