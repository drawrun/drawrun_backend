from django.http import JsonResponse
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
import json
import jwt

from .models import Painting
from .serializers import painting_serializer
from utilities import json_request
from jwtauth.services import authorize
from user.models import User, PausedPainting, CompletedPainting


@json_request
def get_local_popular(request):
    json_data = request.json_data
    if 'offset' in json_data:
        offset = json_data['offset']
    else:
        offset = 0
    if 'amount' in json_data:
        amount = json_data['amount']
    else:
        amount = 500
    try:
        paintings = Painting.objects.order_by('-rating')[offset:offset+amount].get()
    except Painting.DoesNotExist:
        return JsonResponse({'result': []})
    result = []
    if 'fields' in json_data and type(json_data['fields']) == list:
        keys = set(painting_serializer.keys()) & set(json_data['fields'])
    else:
        keys = set(painting_serializer.keys())
    for painting in paintings:
        serialized_painting = {}
        for key in keys:
            serialized_field = painting_serializer[key](painting.__getattribute__(key))
            serialized_painting[key] = serialized_field
        result.append(serialized_painting)
    return JsonResponse({'result': result})


@json_request
def get_put_delete(request, painting_id):
    json_data = request.json_data
    try:
        painting = Painting.objects.get(id=painting_id)
    except Painting.DoesNotExist:
        return JsonResponse({'error': 'not_found'}, status=404)
    if request.method == 'GET':
        if 'fields' in json_data and type(json_data['fields']) == list:
            keys = set(painting_serializer.keys()) & set(json_data['fields'])
        else:
            keys = set(painting_serializer.keys())
        res = {}
        for key in keys:
            serialized_field = painting_serializer[key](painting.__getattribute__(key))
            res[key] = serialized_field
        return JsonResponse({'result': res})
    elif request.method == 'PUT':
        # TODO проверять токен
        allowed_keys = {
            'name', 'path'
        }
        if 'painting' in json_data and type(json_data['painting']) == dict:
            keys = allowed_keys & set(json_data['painting'].keys())
        else:
            keys = allowed_keys
        painting_data = json_data['painting']
        if 'path' in keys:
            with painting.path.open('wt') as f:
                try:
                    json.dump(painting_data['path'], f)
                except (json.JSONDecodeError, ValidationError):
                    return JsonResponse({'error': 'invalid_input'}, status=400)
            del painting_data['path']
        if 'name' in keys:
            try:
                painting.name = painting_data['name']
            except ValueError:
                return JsonResponse({'error': 'invalid_input'}, status=400)
        painting.save()
        return JsonResponse({'result': 'ok'})
    elif request.method == 'DELETE':
        painting.delete()
        return JsonResponse({'result': 'ok'})
    return JsonResponse({'error': 'method_not_allowed'}, status=405)


def start(request, painting_id):
    try:
        access_token = request.META.get('HTTP_ACCESS_TOKEN')
        if access_token is None:
            return JsonResponse({'error': 'authorization_required'}, status=401)
        user_id = authorize(access_token)
    except (jwt.DecodeError, jwt.InvalidSignatureError):
        return JsonResponse({'error': 'invalid_token'}, status=401)
    except jwt.ExpiredSignatureError:
        return JsonResponse({'error': 'token_expired'}, status=401)
    user = User.objects.get(id=user_id)
    try:
        painting = Painting.objects.get(id=painting_id)
    except Painting.DoesNotExist:
        return JsonResponse({'error': 'not_found'}, status=404)
    if user.current_painting is not None:
        return JsonResponse({'error': 'already_running_other_painting'}, status=400)
    elif len(PausedPainting.objects.filter(user=user, painting=painting)) > 0:
        return JsonResponse({'error': 'this_painting_is_paused'}, status=400)
    elif len(CompletedPainting.objects.filter(user=user, painting=painting)) > 0:
        return JsonResponse({'error': 'this_painting_is_completed'}, status=400)
    user.current_painting = painting
    user.save()
    return JsonResponse({'result': 'ok'})


def pause(request, painting_id):
    try:
        access_token = request.META.get('HTTP_ACCESS_TOKEN')
        if access_token is None:
            return JsonResponse({'error': 'authorization_required'}, status=401)
        user_id = authorize(access_token)
    except (jwt.DecodeError, jwt.InvalidSignatureError):
        return JsonResponse({'error': 'invalid_token'}, status=401)
    except jwt.ExpiredSignatureError:
        return JsonResponse({'error': 'token_expired'}, status=401)
    user = User.objects.get(id=user_id)
    try:
        painting = Painting.objects.get(id=painting_id)
    except Painting.DoesNotExist:
        return JsonResponse({'error': 'not_found'}, status=404)
    if user.current_painting != painting:
        return JsonResponse({'error': 'user_running_different_painting'}, status=400)
    elif user.current_painting is None:
        return JsonResponse({'error': 'not_running_any_painting'}, status=400)
    user.current_painting = None
    user.save()
    paused_p = PausedPainting(user=user, painting=painting)
    paused_p.save()
    return JsonResponse({'result': 'ok'})


def continue_painting(request, painting_id):
    try:
        access_token = request.META.get('HTTP_ACCESS_TOKEN')
        if access_token is None:
            return JsonResponse({'error': 'authorization_required'}, status=401)
        user_id = authorize(access_token)
    except (jwt.DecodeError, jwt.InvalidSignatureError):
        return JsonResponse({'error': 'invalid_token'}, status=401)
    except jwt.ExpiredSignatureError:
        return JsonResponse({'error': 'token_expired'}, status=401)
    user = User.objects.get(id=user_id)
    try:
        painting = Painting.objects.get(id=painting_id)
    except Painting.DoesNotExist:
        return JsonResponse({'error': 'not_found'}, status=404)
    if (PausedPainting.objects.filter(user=user, painting=painting)) != 1:
        return JsonResponse({'error': 'this_painting_is_not_paused'}, status=400)
    elif user.current_painting is not None:
        return JsonResponse({'error': 'user_running_different_painting'}, status=400)
    paused_p = PausedPainting.objects.get(user=user, painting=painting)
    paused_p.delete()
    user.current_painting = painting
    user.save()
    return JsonResponse({'result': 'ok'})


def finish(request, painting_id):
    try:
        access_token = request.META.get('HTTP_ACCESS_TOKEN')
        if access_token is None:
            return JsonResponse({'error': 'authorization_required'}, status=401)
        user_id = authorize(access_token)
    except (jwt.DecodeError, jwt.InvalidSignatureError):
        return JsonResponse({'error': 'invalid_token'}, status=401)
    except jwt.ExpiredSignatureError:
        return JsonResponse({'error': 'token_expired'}, status=401)
    user = User.objects.get(id=user_id)
    try:
        painting = Painting.objects.get(id=painting_id)
    except Painting.DoesNotExist:
        return JsonResponse({'error': 'not_found'}, status=404)
    if user.current_painting is None:
        return JsonResponse({'error': 'not_running_any_painting'}, status=400)
    elif user.current_painting != painting:
        return JsonResponse({'error': 'user_running_different_painting'}, status=400)
    elif len(PausedPainting.objects.filter(user=user, painting=painting)) == 1:
        return JsonResponse({'error': 'this_painting_is_paused'}, status=400)
    elif len(CompletedPainting.objects.filter(user=user, painting=painting)) == 1:
        return JsonResponse({'error': 'this_painting_is_completed'}, status=400)
    completed_p = CompletedPainting(user=user, painting=painting)
    completed_p.save()
    return JsonResponse({'result': 'ok'})


@json_request
def send_geolocations(request, painting_id):
    json_data = request.json_data
    try:
        access_token = request.META.get('HTTP_ACCESS_TOKEN')
        if access_token is None:
            return JsonResponse({'error': 'authorization_required'}, status=401)
        user_id = authorize(access_token)
    except (jwt.DecodeError, jwt.InvalidSignatureError):
        return JsonResponse({'error': 'invalid_token'}, status=401)
    except jwt.ExpiredSignatureError:
        return JsonResponse({'error': 'token_expired'}, status=401)
    user = User.objects.get(id=user_id)
    try:
        painting = Painting.objects.get(id=painting_id)
    except Painting.DoesNotExist:
        return JsonResponse({'error': 'not_found'}, status=404)

    def validate_location(location):
        return 'lat' in location and type(location['lat']) == float and \
               'lon' in location and type(location['lon']) == float and \
               'time' in location and type(location['time']) == int

    if type(json_data) != list or not all(validate_location(location) for location in json_data):
        return JsonResponse({'error': 'invalid_input'}, status=400)
    if not user.current_geolocations:
        user.current_geolocations = ContentFile(json.dumps(json_data), name='geolocations.json')
    else:
        with user.current_geolocations.open('wt') as f:
            prev_data = json.load(f)
            prev_data.extend(json_data)
            json.dump(prev_data, f)
    return JsonResponse({'result': 'ok'})


@json_request
def create(request):
    json_data = request.json_data
    try:
        access_token = request.META.get('HTTP_ACCESS_TOKEN')
        if access_token is None:
            return JsonResponse({'error': 'authorization_required'}, status=401)
        user_id = authorize(access_token)
    except (jwt.DecodeError, jwt.InvalidSignatureError):
        return JsonResponse({'error': 'invalid_token'}, status=401)
    except jwt.ExpiredSignatureError:
        return JsonResponse({'error': 'token_expired'}, status=401)
    user = User.objects.get(id=user_id)
    allowed_keys = {
        'name',
        'experience',
        'author',
        'path'
    }
    if not ('painting' in json_data and type(json_data['painting']) == dict):
        return JsonResponse({'error': 'invalid_input'}, status=400)
    painting_data = json_data['painting']
    keys = set(painting_data.keys()) & allowed_keys
    if 'author' in keys and painting_data['author'] != str(user.id):
        return JsonResponse({'error': 'invalid_input'}, status=400)
    new_painting = {}
    for key in keys:
        if key == 'path':
            value = ContentFile(json.dumps(painting_data[key]), name='path.json')
        elif key == 'author':
            value = User.objects.get(pk=painting_data[key])
        else:
            value = painting_data[key]
        new_painting[key] = value
    try:
        new_p = Painting(**new_painting)
        new_p.save()
    except (TypeError, ValidationError):
        return JsonResponse({'error': 'invalid_input'}, status=400)
    return JsonResponse({'result': 'ok'})
