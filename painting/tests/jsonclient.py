from django.test import Client
import json


class JsonClient(Client):
    def json_post(self, path, obj, **kwargs):
        response = self.generic('POST', path, json.dumps(obj), 'application/json', **kwargs)
        # response = self.client.post(path, json.dumps(obj), 'application/json', **kwargs)
        return json.loads(
            response.content.decode('utf-8')
        )

    def json_get(self, path, obj, **kwargs):
        response = self.generic('GET', path, json.dumps(obj), 'application/json', **kwargs)
        # response = self.client.get(path, json.dumps(obj), **kwargs)
        return json.loads(
            response.content.decode('utf-8')
        )

    def json_put(self, path, obj, **kwargs):
        response = self.generic('PUT', path, json.dumps(obj), 'application/json', **kwargs)
        # response = self.client.put(path, json.dumps(obj), 'application/json', **kwargs)
        return json.loads(
            response.content.decode('utf-8')
        )

    def json_delete(self, path, obj, **kwargs):
        response = self.generic('DELETE', path, json.dumps(obj), 'application/json', **kwargs)
        # response = self.client.delete(path, json.dumps(obj), 'application/json', **kwargs)
        return json.loads(
            response.content.decode('utf-8')
        )
