from django.test import TestCase
from django.core.files.base import ContentFile
import json

from painting.models import Painting
from user.models import User, PausedPainting, CompletedPainting
from .jsonclient import JsonClient


class PaintingTestCase(TestCase):
    client_class = JsonClient

    def setUp(self) -> None:
        self.path = [
            {
                'lat': 54.234352342,
                'lon': 45.234235235,
            },
            {
                'lat': 55.234352342,
                'lon': 44.234235235,
            },
            {
                'lat': 53.234352342,
                'lon': 41.234235235,
            },
        ]
        self.user = User(
            username='Goshan',
            name='Georgiy',
            surname='Surkov',
            email='georgiy@example.com'
        )
        self.password = 'jkvnsefms7234!'
        self.user.set_password(self.password)
        self.painting = Painting(
            name='Dyno',
            experience=100,
            author=self.user,
            path=ContentFile(json.dumps(self.path), name='path.json')
        )
        self.user.save()
        self.painting.save()

    def login(self):
        return self.client.json_post(
            f'/account/login/',
            {
                'login': self.user.email,
                'password': self.password
            }
        )['access_token']

    def test_get(self):
        painting_id = self.painting.id
        response = self.client.json_get(
            f'/painting/{painting_id}/',
            {
                'fields': [
                    'id',
                    'name',
                    'experience',
                    'author',
                    'path',
                    'rating',
                    'lat',
                    'lon',
                ]
            }
        )
        expected_res = {
            'result': {
                'id': str(painting_id),
                'name': self.painting.name,
                'experience': self.painting.experience,
                'author': str(self.user.id),
                'path': self.path,
                'rating': 0,
                'lat': self.path[0]['lat'],
                'lon': self.path[0]['lon'],
            }
        }
        self.assertEqual(response, expected_res)

    def test_post(self):
        access_token = self.login()
        obj = {
            'painting': {
                'name': 'painting2',
                'experience': 2020,
                'author': str(self.user.id),
                'path': [
                    {
                        'lat': 45.2345235,
                        'lon': 65.12345235,
                    },
                    {
                        'lat': 43.2345235,
                        'lon': 67.12345235,
                    },
                    {
                        'lat': 41.2345235,
                        'lon': 69.12345235,
                    },
                ]
            }
        }
        response = self.client.json_post(
            '/painting/',
            obj,
            HTTP_ACCESS_TOKEN=access_token
        )
        self.assertEqual({'result': 'ok'}, response)
        painting2 = Painting.objects.filter(name__exact='painting2')[0]
        self.assertEqual(painting2.author.id, self.user.id)

    def test_put(self):
        access_token = self.login()
        response = self.client.json_put(
            f'/painting/{self.painting.id}/',
            {
                'painting': {
                    'name': 'painting2'
                }
            },
            HTTP_ACCESS_TOKEN=access_token
        )
        self.assertEqual({'result': 'ok'}, response)
        self.assertEqual(Painting.objects.get(pk=self.painting.id).name, 'painting2')

    def test_delete(self):
        access_token = self.login()
        response = self.client.json_delete(
            f'/painting/{self.painting.id}/',
            {},
            HTTP_ACCESS_TOKEN=access_token
        )
        with self.assertRaises(Painting.DoesNotExist):
            Painting.objects.get(id=self.painting.id)
        self.assertEqual({'result': 'ok'}, response)

    def test_start_running(self):
        access_token = self.login()
        response = self.client.json_post(
            f'/painting/{self.painting.id}/start/',
            {},
            HTTP_ACCESS_TOKEN=access_token
        )
        self.assertEqual({'result': 'ok'}, response)
        self.user = User.objects.get(pk=self.user.id)
        self.assertEqual(self.user.current_painting, self.painting)

    def test_pause_running(self):
        access_token = self.login()
        self.client.json_post(
            f'/painting/{self.painting.id}/start/',
            {},
            HTTP_ACCESS_TOKEN=access_token
        )
        response = self.client.json_post(
            f'/painting/{self.painting.id}/pause/',
            {},
            HTTP_ACCESS_TOKEN=access_token
        )
        self.user = User.objects.get(pk=self.user.id)
        self.assertEqual({'result': 'ok'}, response)
        self.assertEqual(self.user.current_painting, None)
        self.assertEqual(len(PausedPainting.objects.filter(user=self.user, painting=self.painting)), 1)

    def test_send_geolocations(self):
        access_token = self.login()
        self.client.json_post(
            f'/painting/{self.painting.id}/start/',
            {},
            HTTP_ACCESS_TOKEN=access_token
        )
        response = self.client.json_post(
            f'/painting/{self.painting.id}/send_geolocations/',
            [
                    {'lat': 45.234523, 'lon': 12.53253543, 'time': 1247513217},
            ],
            HTTP_ACCESS_TOKEN=access_token
        )
        self.assertEqual(response, {'result': 'ok'})
