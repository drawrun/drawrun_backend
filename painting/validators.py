from django.core.exceptions import ValidationError
import json


def validate_path_file(value):
    path_error = ValidationError('Invalid path json file')
    try:
        path = json.load(value)
    except json.JSONDecodeError:  # не является валидным json
        raise path_error
    if type(path) != 'list':  # проверям, что это список
        raise path_error
    if len(path) < 2:  # проверяем, что не меньше 2 чекпоинтов
        raise path_error
    is_checkpoint_valid = ['lat' in location and 'lon' in location for location in path]
    if not all(is_checkpoint_valid):  # проверяем валлидность каждого чекпоинта
        raise path_error
