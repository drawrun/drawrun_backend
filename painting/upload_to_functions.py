def upload_thumbnail50(instance, filename):
    file_extension = filename.rsplit('.', 1)[-1]
    return f'painting/{str(instance.id)}/thumbnail50.{file_extension}'


def upload_thumbnail200(instance, filename):
    file_extension = filename.rsplit('.', 1)[-1]
    return f'painting/{str(instance.id)}/thumbnail200.{file_extension}'


def upload_path(instance, filename):
    return f'painting/{str(instance.id)}/path.json'
