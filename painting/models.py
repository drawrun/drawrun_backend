from django.db import models
from django.contrib.auth import get_user_model
import uuid
import json

from .validators import validate_path_file
from .upload_to_functions import upload_thumbnail50, upload_thumbnail200, upload_path
from storages import avatar_storage, private_storage


class Painting(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    experience = models.PositiveIntegerField()
    thumbnail50 = models.ImageField(upload_to=upload_thumbnail50, null=True, storage=avatar_storage)
    thumbnail200 = models.ImageField(upload_to=upload_thumbnail200, null=True, storage=avatar_storage)
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_NULL,
        null=True,
        related_name='paintings_authored'
    )
    path = models.FileField(upload_to=upload_path, validators=[validate_path_file], storage=private_storage)
    rating = models.PositiveSmallIntegerField(default=0)
    lat = models.FloatField()
    lon = models.FloatField()

    def save(self, *args, **kwargs):
        if (self.lat is None or self.lon is None) and self.path is not None:
            path_json = json.load(self.path)
            self.lat = path_json[0]['lat']
            self.lon = path_json[0]['lon']
        super(Painting, self).save(*args, **kwargs)
